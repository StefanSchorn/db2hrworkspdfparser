# -*- coding: iso-8859-15 -*-
__author__ = 'Stefan Schorn - ich@stefanschorn.de'

import os
from cStringIO import StringIO
import xml.etree.ElementTree as ET
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfpage import PDFTextExtractionNotAllowed
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.converter import XMLConverter
import csv
import datetime

dt = datetime.datetime.now()
csvfile = open('HRWorksImport.' + dt.strftime('%Y%m%d_%H%M%S') + '.csv', 'wb')
csvwriter = csv.writer(csvfile, delimiter=';')

def read_folder(folder_name):
    dir_list = os.listdir(folder_name)
    file_list = []
    for file_name in dir_list:
        if file_name.endswith(".pdf"):
            print(file_name)
            file_list.append(file_name)
    return file_list


def get_full_path(file_name):
    return file_name


def parse_pdf_to_xml(full_file_name):
    fp = open(full_file_name, 'rb')
    # Create a PDF parser object associated with the file object.
    parser = PDFParser(fp)
    # Create a PDF document object that stores the document structure.
    # Supply the password for initialization.
    document = PDFDocument(parser)
    # Check if the document allows text extraction. If not, abort.
    if not document.is_extractable:
        raise PDFTextExtractionNotAllowed
    # Create a PDF resource manager object that stores shared resources.
    rsrcmgr = PDFResourceManager()
    # Create a PDF device object.
    retstr = StringIO()
    device = XMLConverter(rsrcmgr, retstr)
    # Create a PDF interpreter object.
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    # Process each page contained in the document.
    for page in PDFPage.create_pages(document):
        interpreter.process_page(page)
    device.write_footer()
    return retstr


def parse_xml(xml_string):
    bbox_old = ["0", "0", "0", "0"]
    root = ET.fromstring(xml_string)
    page_nodes = root.findall("./page")
    first = True
    text = ""
    texts = []
    for page_node in page_nodes:
        text_nodes = page_node.findall("./text")
        for text_node in text_nodes:
            bbox_string = text_node.attrib["bbox"]
            bbox = bbox_string.split(",")
            if bbox[1] != bbox_old[1] and bbox[3] != bbox_old[3]:
                bbox_old = bbox
                if first:
                    first = False
                else:
                    texts.append(text)
                    text = ""
            text += text_node.text
    texts.append(text)
    return texts


def render_to_float(str):
    temp = str.replace(",", "#")
    temp = temp.replace(".", ",")
    temp = temp.replace("#", ".")
    return float(temp)


def parse_texts(texts):
    to = False
    to_text = ""
    date_text = ""
    order_text = ""
    sum_text = ""
    price_text = ""
    tax_amount_text = ""
    tax_text = ""
    for text in texts:
        if to:
            to = False
            to_text = text
        if text.startswith("Hinfahrt:") and to_text == "":
            to = True
        elif text.startswith(u"G�ltigkeit:") and date_text == "":
            date_text = text[-10:]
        elif text.startswith("Auftragsnummer:") and order_text == "":
            order_text = text[-6:]
        elif text.startswith(u"Summe") and sum_text == "":
            sum_text = text[5:]
            prices = sum_text.split(u"�")
            price_text = prices[0]
            tax_amount_text = prices[2]
            tax = render_to_float(tax_amount_text) / render_to_float(price_text)
            if tax < 0.1:
                tax_text = "7%"
            else:
                tax_text = "19%"
    print to_text, "\t", date_text, "\t", order_text, "\t", price_text, "\t", tax_amount_text, "\t", tax_text
    write_to_excel(to_text, date_text, order_text, price_text, tax_amount_text, tax_text)


def write_to_excel(to_text, date_text, order_text, price_text, tax_amount_text, tax_text):
    global line
    global csvwriter
    type = ""
    if (tax_text == "7%"):
        tax_type = "D - Erm��igte Mehrwertsteuer"
        type = "4"
    else:
        tax_type = "D - Volle Mehrwertsteuer"
        type = "5"
    csvwriter.writerow(["Beleg", date_text, to_text + " " + order_text, "EUR", price_text , "1", price_text, tax_type, date_text, type, "1", "", ""])
    line += 1


line = 0
file_list = read_folder(".\\")
for file_name in file_list:
    xml_string = parse_pdf_to_xml(get_full_path(file_name))
    texts = parse_xml(xml_string.getvalue())
    parse_texts(texts)
csvfile.close()